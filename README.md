This repository is part of PlatformCommander, follow this link for more information: https://gitlab.com/KWM-PSY/platform-commander

# Julia configuration

Download the following repositories and unzip them to the a directory of your liking

- [MOOG](https://gitlab.com/KWM-PSY/moog) (this package defines the messages ID that are implemented on the server and a call to a network socket implemented in C)
- [MOO-C-EMU](https://gitlab.com/KWM-PSY/moo-c-emu) (this package defines the messages ID that are implemented on the server and a call to a network socket implemented in C)
- [MoogCom](https://gitlab.com/KWM-PSY/moogcom) (this is the module that implements the communication with the server)
- [Exp_helper](https://gitlab.com/KWM-PSY/exp_helper) (this module defines functions and structures that support an efficient implementation of experiments)
- [Questionnaires](https://gitlab.com/KWM-PSY/questionnaires) (this module is thought to implement Questionnaires frequently used)


* Install julia by either following the instructions on their website [julia](https://julialang.org/downloads/platform/) or executing this command (the julia version installed through the terminal is usually an older version)
    ```
    sudo apt-get install julia
    ```
* Change the directory to /etc/julia/
    ```
    cd /etc/julia/
    ```
* Open the file startup.jl with your prefered editor and add the pathways
    ```
    sudo nano startup.jl
    ```
* Now add the following pathways:




* Open a new terminal and execute julia. You should see this
    ```
        _       _ _(_)_    | Documentation: https://docs.julialang.org
       (_)     | (_) (_)   |
        _ _ _ _| |_ __ _   | Type "?" for help, "]?" for Pkg help.
       | | | | | | |/ _` | |
       | | |_| | | | (_| | | Version 1.6.3-DEV.71 (2019-08-29)
      _/ | __'_|_|_| __'_| | Commit 4bf946a9ca (7 days old master)
     |__/                  |
    julia>
    ```
